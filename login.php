<?php
session_start();

require_once('includes.php');
if (hasActiveSession()) {
    redirect('profile.php');
}
?>

<!DOCTYPE html>
<html>

<body>
<h2> Log In </h2>
<form action="auth.php" method="post">
    <?php
    require_once('includes.php');
    if (isset($_SESSION['err'])) {
        echo '<p><span style="color: #ff0000;">' . $_SESSION['err'] . '</span></p>';
        clearErr();
    }
    ?>
	Username: <br>
    <input type = "text" name = "user">
    <br>
    Password: <br>
    <input type  = "password" name = "password">
    <br><br>
    <input type = "submit" value = "Submit">
    <br><br>
    <p><a href = "newAccount.php"> New? Create an account!</a></p>
</form>    
</body>

</html>