﻿<!doctype html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title>Linking select controls in the lightbox</title>
</head>
<script src="codebase/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="codebase/dhtmlxscheduler.css" type="text/css" charset="utf-8">


<style type="text/css">
    html, body {
        margin: 0px;
        padding: 0px;
        height: 100%;
        overflow: hidden;
    }

    .header {
	background-color: #f1f1f1;
    padding: 20px;
    text-align: center;
}
        .topnav {
	overflow: hidden;
    background-color: #333;
}
        .topnav a {
	float: left;
    display: block;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

.topnav a:hover {
	background-color: #ddd;
    color: black;
}
</style>

<script type="text/javascript" charset="utf-8">
	function init() {
		scheduler.config.multi_day = true;
		scheduler.config.mark_now = true;
		scheduler.config.xml_date="%Y-%m-%d %H:%i";

		scheduler.locale.labels.section_Progress = "Progress section";
		scheduler.locale.labels.section_Worked = "Worked section";
		scheduler.locale.labels.section_priority = "Priority";
		scheduler.locale.labels.section_group = "Group";

		var priority_select = [
			{ key: 'p4', label: "High" },
			{ key: 'p5', label: "Medium" },
			{ key: 'p6', label: "Low" }
		];

		var Progress_select_options = [
			{ key: 'p1', label: "Not Started"},
			{ key: 'p2', label: "In Progress"},
			{ key: 'p3', label: "Done"}
		];
		var Worked_select_options = {
		    p1: [
				{}
		    ],
			p2: [
				{ key: 3, label: "Worked On Today" },
				{ key: 4, label: "Not Worked On Today" }
			],
			p3: [
				{}
			]
		};
		var group_select = [
			{ key: 'p7', label: " " },
			{ key: 'p8', label: " " },
			{ key: 'p9', label: " " },
            { key: 'p10', label: " " },
            { key: 'p11', label: " " },
            { key: 'p12', label: " " },
            { key: 'p13', label: " " },
		];

		var update_select_options = function(select, options) { // helper function
			select.options.length = 0;
			for (var i=0; i<options.length; i++) {
				var option = options[i];
				select[i] = new Option(option.label, option.key);
			}
		};

		var Progress_onchange = function(event) {
			var new_Worked_options = Worked_select_options[this.value];
			update_select_options(scheduler.formSection('Worked').control, new_Worked_options);
		};
		scheduler.attachEvent("onBeforeLightbox", function(id){
			var ev = scheduler.getEvent(id);
			if (!ev.Worked_id) {
				var Progress_id = ev.Progress_id||Progress_select_options[0].key;
				var new_Worked_options = Worked_select_options[Progress_id];
				update_select_options(scheduler.formSection('Worked').control, new_Worked_options);
			}
			return true;
		});

		scheduler.config.lightbox.sections=[
			{ name: "description", height: 130, map_to: "text", type: "textarea", focus: true },
            { name: "Group", height: 23, type: "select", options: group_select, map_to: "group_id" },
            { name: "priority", height: 23, type: "select", options: priority_select, map_to: "priority_id" },
			{name:"Progress", height:24, type:"select", options: Progress_select_options, map_to:"Progress_id", onchange:Progress_onchange },
			{name:"Worked", height:23, type:"select", options: Worked_select_options, map_to:"Worked_id" },
			{name:"time", height:72, type:"time", map_to:"auto"}
		];

		scheduler.init('scheduler_here',new Date(),"week");

		scheduler.parse([
			{ id: 1, start_date: "2017-03-27 09:00", end_date: "2017-03-27 12:00", text:"Task A-12458", Progress_id: 'p3' },
			{ id: 2, start_date: "2017-03-28 09:00", end_date: "2017-03-28 12:00", text:"Task C-788", Progress_id: 'p1' }
		],"json");

		scheduler.showLightbox(1);

	}
</script>

<body onload="init();">
    <div class="topnav">
        <a href="#">My Account</a>
        <a href="#">Home</a>
        <a href="#">Log Out</a>

    </div>
    <div id="scheduler_here" class="dhx_cal_container" style='width:100%; height:100%;'>
        <div class="dhx_cal_navline">
            <div class="dhx_cal_prev_button">&nbsp;</div>
            <div class="dhx_cal_next_button">&nbsp;</div>
            <div class="dhx_cal_today_button"></div>
            <div class="dhx_cal_date"></div>
            <div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
            <div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
            <div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
        </div>
        <div class="dhx_cal_header">
        </div>
        <div class="dhx_cal_data">
        </div>
    </div>
</body>