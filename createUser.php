<?php
session_start();
require_once('includes.php');

$username = $_POST['username'];
$password = $_POST['password'];

if (!isValidPassword($password)) {
    redirect("newAccount.php");
}

$password = hash('SHA256', $password);
runQuery('CALL createPUSER(?, ?)', [$username, $password]);

// Back to login page after account creation
redirect("login.php");

?>