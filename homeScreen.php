﻿<?php
session_start();
?>

<!DOCTYPE html>
<html lang = "en">
<head>
<title>CSS Website Layout</title>
<meta charset = "utf-8">
<meta name = "viewport" content = "with = devicce - width, initial - scale = 1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style>


body{
	margin: 0;
}

.header {
	background-color: #f1f1f1;
    padding: 20px;
    text-align: center;
}

.topnav {
	overflow: hidden;
    background-color: #333;
}

.topnav a {
	float: left;
    display: block;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

.topnav a:hover {
	background-color: #ddd;
    color: black;
}

.column {
    float: left;
    width: 100%;
    padding: 15px;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
@media screen and (max-width:600px) {
    .column {
        width: 100%;
    }
}

.footer {
    background-color: #f1f1f1;
    padding: 10px;
    text-align: center;
}

.fa {
	padding: 10px;
    font-size: 20px;
    width: 30px;
    text-align: center;
    text-decoration: none;
    margin 5px 2px;
    
}

.fa:hover {
	opacity: 0.7;
}

.fa-facebook {
	background: #3B5998;
    color: white;
}

.fa-twitter {
	background: #55ACEE;
    color: white;
}

.fa-youtube {
	background: #bb0000;
    color: white;
}

.fa-snapchat-ghost {
  background: #fffc00;
  color: white;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}

</style>
</head>
<body>

<div class = "header">
	<h1>Scheduler</h1>
</div>

<div class = "topnav">
    <?php
    session_start();
    require_once('includes.php');

    if (!hasActiveSession()) {
        echo '<a href = "#">Log in</a>';
    } else {
        echo '<a href="#">My Account</a>
        <a href = "#">Schedule</a> 
        <a href = "#">Log Out</a>';

        if (isAdmin()) {
            echo '<a href = "#">Admin</a>';
        }
    }
    ?>
</div>



<div class="row">
  <div class="column">
    <h2>Title</h2>
    <p>Welcome to the easy to use Scheduler. Where you're only a few clicks away from having your life in order.
      Don't believe me? Try it out for yourself and sign up TODAY!</p>
  </div>
</div>

<div class = "footer">
	<p><a href = "#" class = "fa fa-facebook"></a>
    <a href = "#" class = "fa fa-twitter"></a>
    <a href = "#" class = "fa fa-youtube"></a>
    <a href = "#" class = "fa fa-snapchat-ghost"></a></p>
</div>

</body>
</html>