-- https://stackoverflow.com/questions/41898551/dates-less-than-or-equal-to-6-months-old-but-not-including-dates-today-2-da?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

-- What about:
SELECT * FROM myTable WHERE DATE(myDateTime) >= DATE(NOW() - interval 6 month + interval 2 day))

-- Edit: Just saw you need it to be historical so adjust to:
SELECT * FROM myTable WHERE DATE(myDateTime) BETWEEN DATE(NOW() - interval 6 month + interval 2 day) AND DATE(NOW())