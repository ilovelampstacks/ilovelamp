<?php
session_start();

require_once('includes.php');
if (!hasActiveSession()) {
    redirect('login.php');
} else if (!isAdmin()) {
    redirect('homeScreen.php');
}
?>

<!DOCTYPE html>
<html>

<head>
    <title>HTML dynamic table using JavaScript</title>
    <script type="text/javascript" src="app.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

            input[type=submit]:hover {
                background-color: #45a049;
            }

        div {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
        }

        .header {
            background-color: #f1f1f1;
            padding: 20px;
            text-align: center;
        }

        .topnav {
            overflow: hidden;
            background-color: #333;
        }

            .topnav a {
                float: left;
                display: block;
                color: #f2f2f2;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

                .topnav a:hover {
                    background-color: #ddd;
                    color: black;
                }

        @media screen and (max-width:600px) {
            .column {
                width: 100%;
            }
        }
    </style>
</head>

<body onload="load();">
    <div class="topnav">
        <a href="#">Schedule</a>
        <a href="#">Home</a>
        <a href="#">Log Out</a>
    </div>
    <div id="adminData">
        <b>Everything</b>
        <table id="adminTableData" border="1" cellpadding="3">
            <tr>
                <td></td>
                <td><b>User</b></td>
                <td><b>Task</b></td>
            </tr>
        </table>
        &nbsp;<br />
    </div>
</body>
</html>