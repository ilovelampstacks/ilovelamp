<?php
session_start();
?>

<!DOCTYPE html>
<html>
<style>

body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box}
input[type=text], input[type=password]{
	width: 100%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus{
	background-color: #ddd;
    outline: none;
}    

hr {
	border: 1px solid #f1f1f1;
    margin-bottom: 25px;
}

button, input[type=submit] {
	background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
}

button:hover { 
	opacity:1;
}

.cancelbtn {
	padding: 14px 20px;
    background-color: #f44336;
}

.cancelbtn, .signupbtn {
	float: left;
    width: 50%;
}

.container {
	padding: 16px;
}

.clearfix::after {
	content: "";
    clear: both;
    display: table;
}

#message {
	display:none;
    background: #f1f1f1;
    color: #000;
    posotion: relative;
    padding: 20px;
    margin-top: 10px;
}

#message p {
	padding: 10px 35px;
	font-size: 18px;
}

.valid {
	color: green;
}

.valid:before {
	position: relative;
    left: -35px;
    content: "+";
}

.invalid {
    color: red;
}

.invalid:before {
    position: relative;
    left: -35px;
    content: "-";
}

@media screen and (max-width: 300px) {
	.cancelbtn, .signupbtn {
    	width: 100%;
    }
}
</style>
<body>

<form action="createUser.php" method="post" style="border:1px solid #ccc">
	<div class = "container">
    <h1>Sign up</h1>
    <p>Please fill in this form to create an account.</p>
    <hr>
    
    <label for = "username"><b>Username</b></label>
    <input type = "text" placeholder = "Enter Username" name = "username" required>
    
    <label for="password"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" id="password" required>
    
    <div id="message">
  <h3>Passphrase must contain the following:</h3>
  <p id="number" class="valid"><b>Not all numbers</b> number</p>
  <p id="space" class="invalid"><b>Cannot be one word</b></p>
  <p id="length" class="invalid">Minimum <b>Must have 8 characters</b></p>
</div>
    
    <label for = "psw-repeat"><b>Repeat Password</b></label>
    <input type = "password" placeholder = "Repeat Password" name = "psw-repeat" required>
        
    <?php
        require_once('includes.php');
        if (isset($_SESSION['err'])) {
            echo '<p><span style="color: #ff0000;">' . $_SESSION['err'] . '</span></p>';
            clearErr();
        }
    ?>
    

    
    <p>By creating an account you agree to our <a href = "terms.html" style = "color:dodgerblue">Terms & Privacy</a>.</p>
    
    <div class = "clearfix">
      <button type = "button" class = "cancelbtn" href="homeScreen.html">Cancel</button>
      <input type = "submit" class = "signupbtn" title="Sign up">
    </div>    
    </div>
</form>

    <script src="register_pw.js"></script>
</body>
</html>
