<?php

function tryLogin($user, $pass) {
    $hashed = hash('SHA256', $pass);
    $isAdmin = runQuery('CALL validateADMIN(?, ?, @out)', [$user, $pass]);
    $isUser = runQuery('CALL validatePUSER(?, ?, @out)', [$user, $pass]);

    if ($userID != null) {
        $token = bin2hex(random_bytes(24));
        runQuery('CALL createSESSION(?, ?)', [$userID, $token], false);

        $_SESSION['auth_token'] = $authToken;
        $_SESSION['admin'] = $isAdmin;
        return $true;
    }
    
    return false;
}

function hasActiveSession() {
    return isset($_SESSION['auth_token']);
}

function isAdmin() {
    if (isset($_SESSION['admin'])) {
        return $_SESSION['admin'];
    }

    return false;
}

function userForToken($authToken) {
    $statement = runQuery('CALL userWithToken(?)', [$authToken]);
    $user = $statement->fetch();
    if ($user == false) {
        return null;
    }

    return $user;
}

function currentUser() {
    if (!hasActiveSession()) {
        return null;
    }

    return userForToken($_SESSION['auth_token']);
}

function isAdmin() {

}

?>