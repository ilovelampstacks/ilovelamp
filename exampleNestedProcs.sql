-- https://stackoverflow.com/questions/11000802/calling-a-stored-procedure-in-a-stored-procedure-in-mysql?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

CREATE PROCEDURE innerproc(OUT param1 INT)
BEGIN
 insert into sometable;
 SELECT LAST_INSERT_ID() into param1 ;
END
-----------------------------------
CREATE PROCEDURE outerproc()
BEGIN
CALL innerproc(@a);
-- // @a gives you the result of innerproc
SELECT @a INTO variableinouterproc FROM dual;
END
-- OUT parameters should help you in getting the values back to the calling procedure.Based on that the solution must be something like this.



-- If it returns a row, this is a stored function and not a stored procedure. You can use something like the following to insert into your table:

INSERT INTO tablename SELECT (SELECT col1, col2 FROM (SELECT somefunction()))
-- Otherwise, it will be a stored procedure and you should do something like this, assuming that @var1 and @var2 are output parameters:

CALL someprocedure(@var1, @var2, @var3)
INSERT INTO tablename SELECT(@var1, @var2)
-- See the documentation about Create Procedure and Create Function for more information about functions versus procedures.