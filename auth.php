<?php
session_start();
require_once('includes.php');

$user = $_POST['user'];
$pass = $_POST['password'];
$pass = hash('SHA256', $pass);

$success = tryLogin($user, $pass);

if ($success) {
    redirect("profile.php");
} else {
    setErr("Invalid login.");
    redirect("login.php");
}

?>