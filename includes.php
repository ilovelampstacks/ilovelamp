<?php

require_once('sql.php');
require_once('api.php');

function setErr($msg = '') {
    $_SESSION['err'] = $msg;
}

function clearErr() {
    $_SESSION['err'] = null;
}

function redirect($dest) {
    header("Location:" . $dest);
    exit;
}

function contains($str, $substr) {
    return strpos($str, $substr) !== false;
}

function containsAny($str, $subs) {
    foreach ($subs as $s) {
        if (contains($str, $s)) {
            return true;
        }
    }
    
    return false;
}

function isValidPassword($pass) {
    $errorMsg = '';
    
    if (strlen($pass) <= '8') {
        $errorMsg .= "Passphrase must be at least 8 characters.\n";
    }
    if (preg_match('^.+?((.)\2{2,}).*$', $pass)) {
        $errorMsg .= "Passphrase must not be gibberish or too repetitive (i.e. 'fooooood').\n";
    }
    if (preg_match('^\d+$', $pass)) {
        $errorMsg .= "Passphrase must not be all numbers.\n";
    }
    if (containsAny($pass, ['\n', '\t'])) {
        $errorMsg .= "Passphrase cannot contain tabs or newlines.\n";
    }
    if (!contains($pass, ' ')) {
        $errorMsg .= "Passphrase must be a phrase consisting of words separated by spaces.\n";
    }
    
    if ($errorMsg != null) {
        setErr(trim($errorMsg));
        return false;
    }
    
    return true;
}



?>