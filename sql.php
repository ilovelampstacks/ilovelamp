<?php
$pdo;

function connect() {
    global $pdo;
    
    $host = '127.0.0.1';
    $db   = 'app';
    $user = 'root';
    $pass = '';
    $charset = 'utf8';

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $opt = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    
    $pdo = new PDO($dsn, $user, $pass, $opt);
    
    // Clear err variable
    $_SESSION['err'] = null;
    
    // Exception options
    ini_set('display_errors', 1);
    ini_set('log_errors', 1);
}

function runQuery($query, $args = [], $out = true) {
    global $pdo;
    if ($pdo == null) {
        connect();
    }
    
    $statement = $pdo->prepare($query);
    $statement->execute($args);
    if ($out) {
        return $pdo->query("select @out;")->fetchAll()[0];
    }

    return $statement;
}









?>