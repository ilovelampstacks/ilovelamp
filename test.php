<?php
session_start();
?>

<!DOCTYPE html>
<html>
<style>

body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box}
input[type=text], input[type=password]{
	width: 100%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus{
	background-color: #ddd;
    outline: none;
}    

hr {
	border: 1px solid #f1f1f1;
    margin-bottom: 25px;
}

button {
	background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
}

button:hover { 
	opacity:1;
}

.cancelbtn {
	padding: 14px 20px;
    background-color: #f44336;
}

.cancelbtn, .signupbtn {
	float: left;
    width: 50%;
}

.container {
	padding: 16px;
}

.clearfix::after {
	content: "";
    clear: both;
    display: table;
}

#message {
	display:none;
    background: #f1f1f1;
    color: #000;
    posotion: relative;
    padding: 20px;
    margin-top: 10px;
}

#message p {
	padding: 10px 35px;
	font-size: 18px;
}

.valid {
	color: green;
}

.valid:before {
	position: relative;
    left: -35px;
    content: "+";
}

.invalid {
    color: red;
}

.invalid:before {
    position: relative;
    left: -35px;
    content: "-";
}

@media screen and (max-width: 300px) {
	.cancelbtn, .signupbtn {
    	width: 100%;
    }
}
</style>
<body>

<form action="createUser.php" style="border:1px solid #ccc">
	<div class = "container">
    <h1>Sign up</h1>
    <p>Please fill in this form to create an account.</p>
    <hr>
    
    <label for = "username"><b>Username</b></label>
    <input type = "text" placeholder = "Enter Username" name = "username" required>
    
    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" id="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
    
    <div id="message">
  <h3>Password must contain the following:</h3>
  <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
  <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
  <p id="number" class="invalid">A <b>number</b></p>
  <p id="length" class="invalid">Minimum <b>8 characters</b></p>
</div>

<script>
var myInput = document.getElementById("psw");
var letter = document.getElementById("letter");
var capital = document.getElementById("capital");
var number = document.getElementById("number");
var length = document.getElementById("length");

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
    document.getElementById("message").style.display = "block";
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
    document.getElementById("message").style.display = "none";
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove("invalid");
    letter.classList.add("valid");
  } else {
    letter.classList.remove("valid");
    letter.classList.add("invalid");
  }
  
  // Validate capital letters
  var upperCaseLetters = /[A-Z]/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove("invalid");
    capital.classList.add("valid");
  } else {
    capital.classList.remove("valid");
    capital.classList.add("invalid");
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove("invalid");
    number.classList.add("valid");
  } else {
    number.classList.remove("valid");
    number.classList.add("invalid");
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove("invalid");
    length.classList.add("valid");
  } else {
    length.classList.remove("valid");
    length.classList.add("invalid");
  }
}
</script>
    
    <label for = "psw-repeat"><b>Repeat Password</b></label>
    <input type = "password" placeholder = "Repeat Password" name = "psw-repeat" required>
        
    <?php
        if (isset($_SESSION['err'])) {
            echo '<p><span style="color: #ff0000;">' . $_SESSION['err'] . '</span></p>';
            clearErr();
        }
    ?>
    
    <label>
    <input type = "checkbox" checked = "checked" name = "remember" style = "margin-bottom:15px"> Remember me 		</label>
    
    <p>By creating an account you agree to our <a href = "terms.html" style = "color:dodgerblue">Terms & Privacy</a>.</p>
    
    <div class = "clearfix">
      <button type = "button" class = "cancelbtn" href="homeScreen.html">Cancel</button>
      <button type = "submit" class = "signupbtn">Sign up</button>
    </div>    
    </div>
</form>

</body>
</html>
