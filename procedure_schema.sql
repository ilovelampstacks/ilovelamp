# MAKE SURE TO CHANGE YOUR DELIMITER TO // BEFORE CREATING THE PROC,
# AND CHANGE IT BACK TO ; AFTER CREATING THE PROC!!!!

###########################################################################
# CREATIONAL
###########################################################################
# create ADMIN
CREATE PROCEDURE createADMIN (IN uname VARCHAR(255), IN pswrd VARCHAR(255)) 
BEGIN INSERT INTO ADMINS (Username, Password) VALUES(uname, pswrd); END //

# create PUSER
CREATE PROCEDURE createPUSER (IN uname VARCHAR(255), IN pswrd VARCHAR(255)) 
BEGIN INSERT INTO PUSERS (Username, Password) VALUES(uname, pswrd); END //

# create GROUP
CREATE PROCEDURE createGROUP (IN owner VARCHAR(255), IN gname VARCHAR(255), IN taskID INT) 
BEGIN INSERT INTO GROUPS (GroupName, Owner) VALUES(gname, owner); END //

# create TASK
CREATE PROCEDURE createTASK (IN tname VARCHAR(255), IN cat VARCHAR(255), due TIMESTAMP, gID INT, priority VARCHAR(255)) 
BEGIN INSERT INTO TASKS (TaskName, Category, DueDate, GroupID, Priority) VALUES(tname, cat, due, gID, priority); END //

# create CATEGORY
CREATE PROCEDURE createCATEGORY (IN cname VARCHAR(255))
BEGIN INSERT INTO CATEGORIES (CatName) VALUES(cname); END //

# create DATES
CREATE PROCEDURE createDATES (IN dday TIMESTAMP, lastday TIMESTAMP)
BEGIN INSERT INTO DATES (DueDay, DateLimit) VALUES(dday, lastday); END //

# create SESSION
CREATE PROCEDURE createSESSION (IN uname INT, session_token VARCHAR(24))
BEGIN INSERT INTO SESSIONS (SessionPUID, Token) VALUES(uname, session_token); END //

###########################################################################
# ACCOUNT QUERIES
###########################################################################
# query if admin info is in the table
CREATE PROCEDURE findADMIN (IN uname VARCHAR(255), OUT result INT)
BEGIN SELECT AdminID FROM ADMINS WHERE Username = uname INTO result; END //

# query if admin info is in the table and if the password is correct
CREATE PROCEDURE validateADMIN (IN uname VARCHAR(255), IN pword VARCHAR(255), OUT result INT)
BEGIN SELECT COUNT(*) FROM ADMINS WHERE Username = uname, Password = pword INTO result; END //

# query to get PUSER row associated with auth Token
CREATE PROCEDURE userWithToken (IN authToken VARCHAR(255))
BEGIN SELECT * from PUSERS NATURAL JOIN SESSIONS WHERE Token = authToken; END //

# query if PUSER info is in the table
CREATE PROCEDURE findPUSER (IN uname VARCHAR(255), OUT result INT)
BEGIN SELECT PUserID FROM PUSERS WHERE Username = uname INTO result; END //

# query if PUSER info is in the table and if the password is correct
CREATE PROCEDURE validatePUSER (IN uname VARCHAR(255), IN pword VARCHAR(255), OUT result INT)
BEGIN SELECT COUNT(*) FROM PUSERS WHERE Username = uname, Password = pword INTO result; END //


###########################################################################
# GROUP QUERIES
###########################################################################
# query to get groupID by name
CREATE PROCEDURE findGROUP (IN gname VARCHAR(255), OUT result INT)
BEGIN SELECT GroupID FROM GROUPS WHERE GroupName = gname INTO result; END //

# query to get groupID by name
CREATE PROCEDURE findGroupOwner (IN gID INT, OUT result VARCHAR(255))
BEGIN SELECT Owner FROM GROUPS WHERE GroupID = gID INTO result; END //


# query to add user to a group
CREATE PROCEDURE addToGroup (IN gID INT, IN uID INT)
BEGIN INSERT INTO MEMBERSHIPS VALUES( gID, uID); END //

###########################################################################
# TASK QUERIES
###########################################################################
# see when the last time a task's progress was updated
# NOTE: we have to make sure that every time a user changes the priority, we call update to the task!!
CREATE PROCEDURE taskLastEdited (IN taskid INT, OUT lastupdate TIMESTAMP)
BEGIN SELECT Updated_At FROM TASKS WHERE TaskID = taskid INTO result; END //

# update task's Progress and Updated_At
CREATE PROCEDURE updateTask (IN taskid INT, IN prog INT)
BEGIN UPDATE TASKS SET Progress = prog WHERE TaskID = taskid; END //

# find a task to be able to share it
CREATE PROCEDURE findTaskID (IN taskname INT, IN groupid INT, OUT result INT)
BEGIN SELECT TaskID FROM TASKS WHERE TaskName = taskname, GroupID = groupid INTO result; END //

# share same task with another group
CREATE PROCEDURE shareTask (IN taskid INT, IN groupid INT)
BEGIN INSERT INTO TASKS (TaskName, Category, DueDate, Priority, Progress, Updated_At) 
VALUES (SELECT TaskName, Category, DueDate, Priority, Progress, Updated_At FROM TASKS WHERE TaskID = taskid);
UPDATE TASKS SET GroupID = groupid WHERE TaskID != taskid, TaskName = (SELECT TaskName FROM TASKS WHERE TaskID = taskid));

# reschedule due date for High Priority task not completed
# also may need to run the "demote oldest task if >3 high priority on same day or if >10 med in category"
CREATE PROCEDURE rescheduleTask (IN taskid INT, IN next_due TIMESTAMP)
BEGIN SELECT TaskID as LateTask FROM TASKS WHERE PriorityLvl = "High", Progress != "Complete", DueDate < NOW()); 
UPDATE TASKS SET DueDate = NOW() + INTERVAL 1 DAY WHERE TaskID = LateTask; END //

# query to update the earliest made task to demote the Priority by 1 level when >3 HIGH tasks due on same day
CREATE PROCEDURE demoteTaskPriority ()
BEGIN IF (SELECT COUNT(*) FROM TASKS WHERE PriorityLvl = "High" GROUP BY DueDate) > 3
THEN (UPDATE TASKS SET PriorityLvl = Medium WHERE min(TaskID)); END //

###########################################################################
# CATEGORY's TASKS QUERIES
###########################################################################
# query if the same TaskName occurs more than 1x in a category
CREATE PROCEDURE checkCategoryTasks (IN taskname VARCHAR(255), IN cat VARCHAR(255), OUT result INT, OUT taskid INT)
BEGIN SELECT COUNT(*), TaskID FROM CATEGORIES WHERE TaskName = taskname, Category = cat INTO result, taskid; END //

# query if there are more than 10 Medium Priorities
# priorities are in the TASK not the CATEGORY
CREATE PROCEDURE checkCategoryPriorities (IN cat VARCHAR(255), IN pri INT, OUT result INT)
BEGIN SELECT COUNT(*) FROM CATEGORIES WHERE CatName = cat, PriorityLvl = pri INTO result; END //

# query to demote earliest created task priority to low if any 1 category has >10 Medium priority tasks
CREATE PROCEDURE demoteTaskPriority ()
BEGIN IF (SELECT COUNT(*) FROM TASKS WHERE PriorityLvl = "Medium" GROUP BY DueDate) > 3
THEN (UPDATE TASKS SET PriorityLvl = Medium WHERE min(TaskID)); END //












