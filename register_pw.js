var myInput = document.getElementById("password");
var number = document.getElementById("number");
var space = document.getElementById("space");
var length = document.getElementById("length");

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
    document.getElementById("message").style.display = "block";
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
    document.getElementById("message").style.display = "none";
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  
  // Validate numbers
  var notAllNumbers = /^\d+$/.test(myInput.value);
  if(notAllNumbers) {  
    number.classList.remove("valid");
     number.classList.add("invalid");
  } else {
    number.classList.remove("invalid");
    number.classList.add("valid");
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove("invalid");
    length.classList.add("valid");
  } else {
    length.classList.remove("valid");
    length.classList.add("invalid");
  }

    // Validates space
  if (myInput.value.indexOf(' ') != -1) {
      space.classList.remove("invalid");
      space.classList.add("valid");
  } else {
      space.classList.remove("valid");
      space.classList.add("invalid");
  }
}