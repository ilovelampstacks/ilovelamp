information_schema.UPDATE_TIME

-- https://stackoverflow.com/questions/307438/how-can-i-tell-when-a-mysql-table-was-last-updated?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
-- I'm surprised no one has suggested tracking last update time per row:

mysql> CREATE TABLE foo (
  id INT PRIMARY KEY
  x INT,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
                     ON UPDATE CURRENT_TIMESTAMP,
  KEY (updated_at)
);

mysql> INSERT INTO foo VALUES (1, NOW() - INTERVAL 3 DAY), (2, NOW());

mysql> SELECT * FROM foo;
-- +----+------+---------------------+
-- | id | x    | updated_at          |
-- +----+------+---------------------+
-- |  1 | NULL | 2013-08-18 03:26:28 |
-- |  2 | NULL | 2013-08-21 03:26:28 |
-- +----+------+---------------------+
mysql> UPDATE foo SET x = 1234 WHERE id = 1;
-- This updates the timestamp even though we didn't mention it in the UPDATE.

-- mysql> SELECT * FROM foo;
-- +----+------+---------------------+
-- | id | x    | updated_at          |
-- +----+------+---------------------+
-- |  1 | 1235 | 2013-08-21 03:30:20 | <-- this row has been updated
-- |  2 | NULL | 2013-08-21 03:26:28 |
-- +----+------+---------------------+

-- Now you can query for the MAX():
mysql> SELECT MAX(updated_at) FROM foo;
-- +---------------------+
-- | MAX(updated_at)     |
-- +---------------------+
-- | 2013-08-21 03:30:20 |
-- +---------------------+
-- Admittedly, this requires more storage (4 bytes per row for TIMESTAMP).
-- But this works for InnoDB tables before 5.7.15 version of MySQL, which INFORMATION_SCHEMA.TABLES.UPDATE_TIME doesn't.